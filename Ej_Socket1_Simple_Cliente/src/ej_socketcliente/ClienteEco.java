package ej_socketcliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClienteEco {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Usar: java EchoClient <host> <puerto>");
            System.exit(1);
        }
        
        System.out.println("Ingresar un texto por teclado: ");
        //String hostName = args[0];
        String hostName = "190.12.191.57";
        int numeroPuerto = 1020;
        //int numeroPuerto = Integer.parseInt(args[1]);
        try (
                Socket socketEco = new Socket(hostName, numeroPuerto);
                PrintWriter out = new PrintWriter(socketEco.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socketEco.getInputStream()));
                BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {
            String lineaEntrada;
            while ((lineaEntrada = stdIn.readLine()) != null) {
                out.println(lineaEntrada);
                System.out.println("echo: " + in.readLine());
            }
        } catch (UnknownHostException e) {
            System.err.println("No se encuentra host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No se puedo conectar a host/puerto " + hostName);
            System.exit(1);
        }
    }
}
