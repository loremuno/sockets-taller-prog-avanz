package ej_socketserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class ServerEco {

    public static void sonido() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
        //tocar el sonido
        AudioInputStream stream;
        AudioFormat format;
        DataLine.Info info;
        Clip clip;
        stream = AudioSystem.getAudioInputStream(new File("bell.wav"));
        format = stream.getFormat();
        info = new DataLine.Info(Clip.class, format);
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(stream);
        clip.start();
    }

    public static void grabarNombre(String linea) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("textos.txt", true);
            pw = new PrintWriter(fichero);
            pw.println(linea);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        if (args.length < 1) {
            System.err.println("Usar: java EchoServer <puerto>");
            System.exit(1);
        }
        System.out.println("Servidor Iniciado. Escuchando en puerto 1020");
        int numeroPuerto = Integer.parseInt(args[0]);
        while (true) {                 //repetir para otro usuario.  TODO:  que hacemos si se solapan ?
            try (
                    ServerSocket socketServer = new ServerSocket(numeroPuerto);
                    Socket clientSocket = socketServer.accept();
                    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));) {
                System.out.println("Cliente conectado en puerto " + numeroPuerto + ". Sirviendo pedidos.");
                String lineaEntrada;
                while ((lineaEntrada = in.readLine()) != null) {

                    try {
                        sonido();
                    } catch (Exception e) {
                    }
                    grabarNombre(lineaEntrada);
                    System.out.println("Recibiendo mensaje: " + lineaEntrada + " de " + clientSocket.toString());
                    out.println("El server dice: Recibido: " + lineaEntrada + "\t\t" + "Ahora tipea tu nombre y apellido; ");
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
